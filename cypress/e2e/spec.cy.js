import faker from 'faker';

const randomName = faker.name.firstName();
const randomEmail = faker.internet.email();
const randomPhone = faker.phone.phoneNumber();
const randomSubject = faker.lorem.words();
const randomMessage = faker.lorem.sentences();
const randomFirstName = faker.name.firstName();

// //test hello world

// describe('template spec', () => {
//   it('passes', () => {
//     cy.visit('https://google.com')
//   })
// })

// //test assert 

// describe('Google search for ISTQB', () => {
//   it('should display ISTQB as the first result', () => {
//     // Visit Google website
//     cy.visit('https://www.google.com/')

//     // Enter search query
//     cy.get('button:contains("accepter")').click()
//     cy.get('[name="q"]').type('ISTQB{enter}')

//     // Assert that the first result contains ISTQB
//     cy.get('div[data-async-context="query:ISTQB"] div:nth-child(1) h3').first()
//     .should('contain.text', 'International Software Testing Qualifications Board');
  
//   })
// })

// //test assert 2

// describe('Google search for Spark Archives', () => {
//   it('should display Spark Archives as the first result', () => {
//     // Visit Google website
//     cy.visit('https://www.google.com/')

//     // Enter search query
//     cy.get('button:contains("accepter")').click()
//     cy.get('[name="q"]').type('Spark Archives{enter}')

//     // Assert that the first result contains ISTQB
//     cy.get('div[data-async-context="query:Spark%20Archives"] div:nth-child(1) h3').first()
//     .should('contain.text', 'Spark');
  
//   })
// })

// //test iframe

describe('Cypress Kitchen Sink app', () => {
  it('should select an option from the select list', () => {
    cy.visit('https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_select');
    cy.get('div#accept-choices').click();
    cy.get('#iframeResult').its('0.contentDocument').should('exist').then(cy.wrap).find('#cars').select('opel').then(() => {
    cy.get('#iframeResult').its('0.contentDocument').should('exist').then(cy.wrap).find('input[type="submit"]').click();
  })
    
  });
});


// describe('Form random site', () => {
//   it('should select an option from the select list', () => {
//     cy.visit('https://www.linscription.com/pro/contact.php');
//     cy.get('select#A46').should('be.visible').select(`${2}`)

//     cy.wait(500).get('#A6').should('be.visible').type(randomName+" Corp");
//     cy.get('#A20').type(randomName);
//     cy.get('#A21').type(randomFirstName);
//     cy.get('#A9').type(randomEmail);
//     cy.get('#A26').type(randomPhone);
//     cy.get('#A1_EDT').its('0.contentDocument').should('exist').then(cy.wrap).find('body').type(randomSubject+"  "+randomMessage);
//   })   
// });